export const ProductService = {
    getAll,
    getOne,
    getImage,
    add,
    deleteProduct,
};

import getApiOrigin from '../helpers/api_origin.js';

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    return fetch(getApiOrigin + `api/products/`, requestOptions)
        .then(handleResponse);
}

function getOne(id) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    return fetch(getApiOrigin + `api/products/` + id, requestOptions)
        .then(handleResponse);
}

function getImage(id) {
    return getApiOrigin + 'api/products/' + id + '/image';
}

function add(product){
    console.log('Add product')
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(product)
    };
    return fetch(getApiOrigin + 'api/products/', requestOptions);
}

function deleteProduct(id){
    console.log('Delete product')
    const requestOptions = {
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'}
    };
    return fetch(getApiOrigin + 'api/products/'+id, requestOptions);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (!response.ok) {
            console.log(response.ok);
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}

export const CartService = {
    getProducts,
    add,
    empty
};
import { getApiOrigin } from "../helpers/api_origin";

function getProducts(email) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    return fetch(getApiOrigin() + 'api/carts/' + email, requestOptions)
        .then(handleResponse);
}

function add(email, payload){
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(payload)
    };
    return fetch(getApiOrigin() + 'api/carts/' + email, requestOptions);
}

function empty(email) {
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    };
    return fetch(getApiOrigin() + 'api/carts/' + email, requestOptions)
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (!response.ok) {
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}
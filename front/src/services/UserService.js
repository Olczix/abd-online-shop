export const UserService = {
    getAll,
    getOne,
    add,
    deleteUser,
};

import { getApiOrigin } from '../helpers/api_origin.js';

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    return fetch(getApiOrigin() + `api/users`, requestOptions)
        .then(handleResponse);
}

function getOne(email) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    return fetch(getApiOrigin() + `api/users` + email, requestOptions)
        .then(handleResponse);
}

function add(user){
    console.log('Add user')
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(user)
    };
    return fetch(getApiOrigin() + 'api/users', requestOptions);
}

function deleteUser(email){
    console.log('Delete product')
    const requestOptions = {
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'}
    };
    return fetch(getApiOrigin() + 'api/users/' + email, requestOptions);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (!response.ok) {
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}
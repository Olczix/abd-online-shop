export const OrderService = {
    getOrders,
    addOrder,
};
import getApiOrigin from '../helpers/api_origin.js';

function getOrders(email) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    return fetch(getApiOrigin + 'api/orders/' + email, requestOptions)
        .then(handleResponse);
}

function addOrder(payload){
    console.log(payload);
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(payload)
    };
    return fetch(getApiOrigin + 'api/orders', requestOptions);
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (!response.ok) {
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}
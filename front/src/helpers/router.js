import { createRouter, createWebHistory } from 'vue-router';
import Home from '../pages/Home.vue';
import AddProduct from '../pages/AddProduct.vue';
import AddUser from '../pages/AddUser.vue'
import Login from '../pages/Login.vue'
import UserCart from '../pages/UserCart.vue';
import UserOrders from '../pages/UserOrders.vue';


const routes = [
    { path: '/home', component: Home },
    { path: '/product/add', component: AddProduct},
    { path: '/signup', component: AddUser},
    { path: '/login', component: Login},
    { path: '/cart', component: UserCart},
    { path: '/orders', component: UserOrders }
    // { path: '*', redirect: '/' },
];


const router = createRouter({
    history: createWebHistory(),
    routes,
})


router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login', '/signup', '/home'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');
  
    if (authRequired && !loggedIn) {
      return next({ 
        path: '/login', 
        query: { returnUrl: to.path } 
      });
    }
  
    next();
  })

export default router;
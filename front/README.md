# front

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
for npm 7.0.0 +
npm run serve -- --port 81
for npm 6.0.0 +
npm run serve --port 81
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

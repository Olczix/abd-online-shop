from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_migrate import Migrate


app = Flask(__name__)
app.config.from_object("website.config.Config")
app.config['SECRET_KEY'] = 'lalalalalalafafafafa'
app.config['CORS_HEADERS'] = 'Content-Type'
CORS(app)


# Initialize DB
db = SQLAlchemy(app)
migrate = Migrate(app, db)


# Register Blueprints
from .controllers.product_controllers import product_views
app.register_blueprint(product_views, url_prefix='/api/products')

from .controllers.user_controllers import user_views
app.register_blueprint(user_views, url_prefix='/api/users')

from .controllers.order_controllers import orders_views
app.register_blueprint(orders_views, url_prefix='/api/orders')

from .controllers.cart_controllers import carts_view
app.register_blueprint(carts_view, url_prefix='/api/carts')

from .controllers.auth_controllers import auth_views
app.register_blueprint(auth_views, url_prefix='/api/auth')

from .controllers.data_orm import data_orm_view
app.register_blueprint(data_orm_view, url_prefix='/api/data/orm')

from .controllers.data_sql import data_sql_view
app.register_blueprint(data_sql_view, url_prefix='/api/data/sql')

from .models.models import User, Admin, Cart, Product

# Prepare DB
def prepare_database():
   db.drop_all()
   db.create_all()
   db.session.commit()

# Initialize DB content
def initialize_database():
   db.drop_all()
   db.create_all()
   db.session.commit()

   db.session.add(Product(name='Summer t-shirt', brand='H&M', description='Gray, summer t-shirt',
      category=1, price=199, size='S', amount=3))
   db.session.add(Admin('admin', 'admin@gmail.com', 'sha256$Us0EOhIhpTYVIJHz$90a0951f06ecd0dd31cdf6af189795ff9dd546f5538bc0585eb40ca46ca4f27b'))
   db.session.add(User('John', 'Doe', 'sha256$aESl6Jceq0eAy1Sr$b92fa75b30ef81cecfcb79bde3edc9643a60ebe70d0d6de26b98704efccbb6ce', 'example@gmail.com', '123456789', 'ul. Gdańska 35/70, Gdańsk'))
   db.session.add(Cart(owner='example@gmail.com'))
   db.session.commit()

import json
import os
import random
import string
from datetime import datetime
from website import db
from sqlalchemy.sql import func
from datetime import timedelta
from flask import Blueprint, jsonify
from website.models.models import User, Product, Order
from static.my_timer import Timer
import psycopg2
from urllib.parse import urlparse


result = urlparse(os.getenv("DATABASE_URL"))
connection = psycopg2.connect(
    user=result.username,
    password=result.password,
    host=result.hostname,
    port=result.port,
    database=result.path[1:]
)

cursor = connection.cursor()

data_sql_view = Blueprint('data_sql', __name__)


@data_sql_view.route('')
def insert():
    with Timer('Insert data - SQL') as t:
        # INSERTS
        products_values = []
        users_values = []
        orders_values = []
        for i in range(100):
            # id, name, brand, description, category, price, size, amount
            products_values.append(
                "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (
                    i,
                    random.choice(string.ascii_uppercase),
                    random.choice(string.ascii_uppercase),
                    random.choice(string.ascii_uppercase),
                    random.choice(['TSHIRT','SHIRT','DRESS', 'TROUSERS']),
                    i*10,
                    random.choice(string.ascii_uppercase),
                    5  
                )              
            )

            # name, surname, password, email, address, phone_number, account_creation_date, 
            # is_account_active, role
            users_values.append(
                "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (
                    random.choice(string.ascii_uppercase),
                    random.choice(string.ascii_uppercase),
                    random.choice(string.ascii_uppercase),
                    f'email_user_{i}@gmail.com',
                    random.choice(string.ascii_uppercase),
                    f'{100000000+i}',
                    '2021-11-28 10:20:34.123456',
                    True,
                    'USER'
                )
            )

            # id, priority, estimated_delivery_time, delivery_address, order_submission_date,
            # status, products, owner
            orders_values.append(
                "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (
                    i,
                    random.choice(['LOW', 'MEDIUM', 'HIGH', 'URGENT']),
                    '2021-12-01 10:30:34.123456',
                    random.choice(string.ascii_uppercase),
                    '2021-11-27 10:20:34.123456',
                    random.choice(['SUBMITTED', 'IN_PROGRESS', 'IN_DELIVERY', 'DELIVERED']),
                    [i+1, i+1],
                    f'email_user_{i}@gmail.com',
                )
            )
        
        insert_products_sql = "INSERT INTO product VALUES"
        insert_users_sql = 'INSERT INTO public."user" VALUES'
        insert_orders_sql = 'INSERT INTO public."order" VALUES'
        products_sql = ",".join(products_values)
        users_sql = ",".join(users_values)
        orders_sql = ",".join(orders_values)
        cursor.execute(f'{insert_products_sql} {products_sql};')
        cursor.execute(f'{insert_users_sql} {users_sql};')
        cursor.execute(f'{insert_orders_sql} {orders_sql};')
        connection.commit()

        query()

    return 'Done', 200

@data_sql_view.route('/query')
def query():
    query_result = []
    with Timer('Select data - SQL') as t1:
        # SELECT
        query = """
            SELECT u.name, u.surname, o.order_submission_date
            FROM public."user" u, public."order" o
            WHERE o.owner = u.email AND o.status != 'DELIVERED' AND o.priority = 'HIGH';
        """
        cursor.execute(query)
        connection.commit()

    return jsonify(query_result), 200

import json
from website import db
from static.utils import is_null
from flask_cors import cross_origin
from flask import Blueprint, jsonify, request
from website.models.models import Cart


carts_view = Blueprint('carts', __name__)


@carts_view.route('')
@cross_origin(origin='localhost', headers=['Content-Type', 'Authorization'])
def carts():
    return jsonify([cart.products for cart in db.session.query(Cart).all()])


@carts_view.route('/<string:user_email>', methods=['GET', 'POST', 'DELETE'])
def cart_by_user(user_email):
    if request.method == 'GET':
        return jsonify(db.session.query(Cart).filter(Cart.owner == user_email) \
            .first().products)

    elif request.method == 'POST':
        if (is_null(request.json['product_id'])):
            return 'Bad request', 404
        
        cart = db.session.query(Cart).filter(Cart.owner == user_email).first()
        products_list = cart.products
        products_list.append(request.json['product_id'])

        db.session.query(Cart)\
            .filter(Cart.owner == user_email)\
            .update({Cart.products: products_list})
        db.session.commit()

        return f"Product (id={request.json['product_id']} added to cart)", 201
    
    elif request.method == 'DELETE':
        cart = db.session.query(Cart).filter(Cart.owner == user_email).first()
        products_list = cart.products
        products_list = []

        db.session.query(Cart)\
            .filter(Cart.owner == user_email)\
            .update({Cart.products: products_list})
        db.session.commit()

        return f"Cart (owner={user_email}) is empty", 200

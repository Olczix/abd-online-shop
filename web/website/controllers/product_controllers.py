from website import db
from static.utils import Category, is_null
from website.models.models import Product
from flask import Blueprint, request, Response, jsonify
from flask_cors import cross_origin


product_views = Blueprint('products', __name__)


@product_views.route('/', methods=['GET', 'POST'])
#@cross_origin(origin='http://localhost', headers=['Content-Type', 'Authorization'])
@cross_origin()
def products():
   if request.method == 'GET':
      return jsonify([product.as_dict() for product in db.session.query(Product).all()])
   
   elif request.method == 'POST':
      if (is_null(request.json['name']) or is_null(request.json['brand']) or 
         is_null(request.json['description']) or is_null(request.json['category']) or 
         is_null(request.json['price']) or is_null(request.json['size']) or 
         is_null(request.json['amount'])):
            return 'Bad request', 404
      db.session.add(Product(name=request.json['name'], brand=request.json['brand'],
         description=request.json['description'], category=request.json['category'],
         price=request.json['price'], size=request.json['size'], amount=request.json['amount']))
      db.session.commit()
      return 'Product created', 201
   

@product_views.route('/<int:product_id>', methods=['GET', 'DELETE'])
@cross_origin(origin='localhost',headers=['Content-Type', 'Authorization'])
def product_by_id(product_id):

   if request.method == 'GET':
      return jsonify(db.session.query(Product).filter(Product.id == product_id). \
         first().as_dict())

   elif request.method == 'DELETE':
      db.session.query(Product).filter(Product.id == product_id).delete()
      db.session.commit()
      return f'Product (id={product_id}) deleted', 200


@product_views.route('/<int:product_id>/image')
def get_product_image_by_id(product_id):
   product_image = db.session.query(Product).filter(Product.id == product_id).first().image
   if not product_image:
      return 'Product Image Not Found!', 404
   return Response(product_image, mimetype='image/jpeg')


@product_views.route('/categories')
@cross_origin(origin='localhost',headers=['Content-Type', 'Authorization'])
def get_categories():
   return jsonify([Category(cat_id).name for cat_id in Category])


@product_views.route('/category/<int:category_id>')
def product_by_category_id(category_id):
   return jsonify([product.as_dict() for product in db.session.query(Product).\
    filter(Product.category == Category(category_id)).all()])


@product_views.route('/category/<string:product_category>')
def product_by_category_name(product_category):
   return jsonify([product.as_dict() for product in db.session.query(Product).\
      filter(Product.category ==  Category[product_category.upper()]).all()])
   
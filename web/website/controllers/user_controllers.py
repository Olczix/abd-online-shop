import json
from website import db
from flask_cors import cross_origin
from static.utils import Roles, is_null
from flask import Blueprint, jsonify, request
from website.models.models import User, Cart
from werkzeug.security import check_password_hash, generate_password_hash


user_views = Blueprint('users', __name__)


@user_views.route('', methods=['GET', 'POST'])
@cross_origin(origin='localhost', headers=['Content-Type', 'Authorization'])
def users():
   if request.method == 'GET':
      return jsonify([user.as_dict() for user in db.session.query(User).all()])

   elif request.method == 'POST':
      if (is_null(request.json['name']) or is_null(request.json['surname']) or 
         is_null(request.json['password']) or is_null(request.json['email']) or 
         is_null(request.json['phone_number']) or is_null(request.json['address'])):
            return 'Bad request', 404

      pwd_hash = generate_password_hash(request.json['password'], method='sha256')
      db.session.add(User(name=request.json['name'], surname=request.json['surname'],
         password=pwd_hash, email=request.json['email'], 
         phone_number=request.json['phone_number'], address=request.json['address']))
      db.session.add(Cart(owner=request.json['email']))
      db.session.commit()
      return 'Account created', 201


@user_views.route('/<string:email>', methods=['GET', 'DELETE'])
def user_by_email(email):
   if request.method == 'GET':
      return jsonify(db.session.query(User).filter(User.email == email)\
         .first().as_dict())
   
   elif request.method == 'DELETE':
      # TODO: Remove Cart deletion after adding cascase delete in User
      # TODO: Remove User orders as well
      db.session.query(Cart).filter(Cart.owner == email).delete()
      db.session.query(User).filter(User.email == email).delete()
      db.session.commit()
      return f'User (email={email}) deleted (cascade)', 200


@user_views.route('/roles', methods=['GET'])
def roles():
   return jsonify([Roles(role).name for role in Roles])

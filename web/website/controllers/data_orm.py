import json
import random
import string
from website import db
from sqlalchemy.sql import func
from datetime import timedelta
from flask import Blueprint, jsonify
from website.models.models import User, Product, Order
from static.helpers import random_date
from static.my_timer import Timer


data_orm_view = Blueprint('data_orm', __name__)


@data_orm_view.route('')
def insert():
    with Timer('Insert data - ORM') as t:
        # INSERTS
        products = []
        users = []
        orders = []
        for i in range(100):
            # name, brand, description, category, price, size, amount, image
            products.append({
                'name': random.choice(string.ascii_uppercase),
                'brand': random.choice(string.ascii_uppercase),
                'description': random.choice(string.ascii_uppercase),
                'category': 1,
                'price': i*10,
                'size': random.choice(string.ascii_uppercase),
                'amount': 5                
            })
            # name, surname, password, email, phone_number, address
            users.append({
                'name': random.choice(string.ascii_uppercase),
                'surname': random.choice(string.ascii_uppercase),
                'password': random.choice(string.ascii_uppercase),
                'email': f'email_user_{i}@gmail.com',
                'phone_number': f'{100000000+i}',
                'address' : random.choice(string.ascii_uppercase)
            })
            # delivery_address, owner, products
            orders.append({
                'delivery_address' : random.choice(string.ascii_uppercase),
                'owner': f'email_user_{i}@gmail.com',
                'products': [i+1, i+1],
                'status': random.choice([1,2,3,4]),
                'priority': random.choice([1,2,3,4])

            })
        db.session.execute(Product.__table__.insert(products))
        db.session.execute(User.__table__.insert(users))
        db.session.execute(Order.__table__.insert(orders))
        db.session.commit()

        query()

    return 'Done', 200

@data_orm_view.route('/query')
def query():
    query_result = []
    with Timer('Select data - ORM') as t1:
        # SELECT
        # week_ago = func.now() - timedelta(days=7)
        query_result = [f'{user.name} {user.surname} ({user.email})' 
            for user in db.session.query(User).join(Order).
            filter(Order.owner == User.email).
            filter(Order.priority == 'HIGH').
            filter(Order.status != 'DELIVERED').
            all()]

    return jsonify(query_result), 200

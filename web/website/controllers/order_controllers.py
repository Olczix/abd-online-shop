import json
from website import db
from static.utils import Priority, OrderStatus, is_null
from flask_cors import cross_origin
from flask import Blueprint, jsonify, request
from website.models.models import Order, Cart, User
from static.utils import OrderStatus, Priority, is_null


orders_views = Blueprint('orders', __name__)


@orders_views.route('', methods=['GET', 'POST'])
@cross_origin(origin='localhost', headers=['Content-Type', 'Authorization'])
def orders():
   if request.method == 'GET':
      return jsonify([order.as_dict() for order in db.session.query(Order).all()])

   elif request.method == 'POST':
      if (is_null(request.json['email']) or is_null(request.json['address'])):
         return 'Bad request', 404

      cart = db.session.query(Cart).filter(Cart.owner == request.json['email']).first()
      db.session.query(Cart).filter(Cart.owner == request.json['email']).update({Cart.products: []})
      db.session.add(Order(owner=request.json['email'], delivery_address=request.json['address'],
                           products=request.json['products']))
      db.session.commit()
      return 'Order created', 201


@orders_views.route('/<string:user_email>', methods=['GET'])
def orders_by_user(user_email):
   if request.method == 'GET':
      return jsonify([order.as_dict() for order in db.session.query(Order) \
         .filter(Order.owner == user_email).all()])


@orders_views.route('/<int:order_id>', methods=['DELETE'])
def delete_user_order(order_id):
      db.session.query(Order).filter(Order.id == order_id).delete()
      db.session.commit()
      return f'Order (id={order_id}) deleted', 200


@orders_views.route('/priorities')
def all_priorities():
   return jsonify([Priority(p).name for p in Priority])


@orders_views.route('/statuses')
def all_order_statuses():
   return jsonify([OrderStatus(s).name for s in OrderStatus])

import json
from website import db
from flask_cors import cross_origin
from static.utils import Roles, is_null
from flask import Blueprint, jsonify, request
from website.models.models import User, Cart, Admin
from werkzeug.security import check_password_hash, generate_password_hash


auth_views = Blueprint('auth', __name__)


@auth_views.route('/login', methods=['POST'])
@cross_origin(origin='localhost', headers=['Content-Type', 'Authorization'])
def login():
    # if not user
    if not db.session.query(User).filter(User.email == request.json['email']).first():
        # if not admin
        if not db.session.query(Admin).filter(Admin.email == request.json['email']).first():
            return 'No e-mail in database', 404

        if not check_password_hash(db.session.query(Admin).
            filter(Admin.email == request.json['email']).first().password, request.json['password']):
            return 'Wrong password', 404

        return jsonify(db.session.query(Admin).
            filter(Admin.email == request.json['email']).first().as_dict()), 200
        
    if not check_password_hash(db.session.query(User).filter(User.email == request.json['email']).
        first().password, request.json['password']):
        return 'Wrong password', 404

    return jsonify(db.session.query(User).
        filter(User.email == request.json['email']).first().as_dict()), 200

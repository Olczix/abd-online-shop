from sqlalchemy.orm import eagerload
from website import db
from random import randint
from datetime import timedelta
from sqlalchemy.sql import func
from flask_login import UserMixin
from static.utils import attributes
from sqlalchemy.dialects.postgresql import JSONB
from static.utils import Category, Roles, Priority, OrderStatus


# Shop product
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    brand = db.Column(db.String(50))
    description = db.Column(db.String(200))
    category = db.Column(db.Enum(Category))
    price = db.Column(db.Float)
    size = db.Column(db.String(5))
    amount = db.Column(db.Integer)

    def __init__(self, name, brand, description, category, price, size, amount) -> None:
        self.name = name
        self.brand = brand
        self.description = description
        self.category = category
        self.price = price
        self.size = size
        self.amount = amount
    
    def as_dict(self):
        return attributes(self)


# Person (Abstract class)
class Person(UserMixin):
    def __init__(self, name, surname, email, password) -> None:
        self.name = name
        self.surname = surname
        self.email = email
        self.password = password
    
    def as_dict(self):
        return attributes(self)
    
    def get_id(self):
           return (self.email)


# Admin
class Admin(db.Model, Person):
    email = db.Column(db.String(50), primary_key=True)
    name = db.Column(db.String(50))
    password = db.Column(db.String(150))
    role = db.Column(db.Enum(Roles))

    def __init__(self, name, email, password) -> None:
        Person.__init__(self, name, '-', email, password)
        self.role = 1
    
    def as_dict(self):
        return Person.as_dict(self)
    
    def get_id(self):
        return Person.get_id(self)


# User
class User(db.Model, Person):
    name = db.Column(db.String(50))
    surname = db.Column(db.String(50))
    password = db.Column(db.String(150))
    email = db.Column(db.String(50), primary_key=True)
    address = db.Column(db.String)
    phone_number =  db.Column(db.String(15), unique=True)
    account_creation_date = db.Column(db.DateTime(timezone=True),
        server_default=func.now())
    is_account_active = db.Column(db.Boolean)
    role = db.Column(db.Enum(Roles))
    # image = db.Column(db.LargeBinary, nullable=True)
    cart = db.relationship('Cart', backref='user', lazy=True, cascade="all, delete")
    orders = db.relationship('Order', backref='user', lazy=True, cascade="all, delete")

    def __init__(self, name, surname, password, email, phone_number, address) -> None:
        Person.__init__(self, name, surname, email, password)
        self.phone_number = phone_number
        self.address = address
        self.is_account_active = True
        self.role = Roles(2).name
        # self.image = image
    
    def as_dict(self):
        return Person.as_dict(self)
    
    def get_id(self):
        return Person.get_id(self)


# Cart
class Cart(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.String, db.ForeignKey('user.email'), nullable=False)
    products = db.Column(JSONB)  # db.relationship('Product', backref='cart', eagerload=True)

    def __init__(self, owner):
        self.owner = owner
        self.products = []
    

# Order
class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    priority = db.Column(db.Enum(Priority))
    estimated_delivery_date = db.Column(db.DateTime(timezone=True))
    delivery_address = db.Column(db.String)
    order_submission_date = db.Column(db.DateTime(timezone=True),
        server_default=func.now())
    status = db.Column(db.Enum(OrderStatus))
    products = db.Column(JSONB) 
    owner = db.Column(db.String, db.ForeignKey('user.email'), nullable=False)

    def __init__(self, delivery_address, owner, products):
        self.owner = owner
        self.delivery_address = delivery_address
        self.products = products
        self.priority = Priority.LOW
        self.status = OrderStatus.SUBMITTED
        self.estimated_delivery_date = func.now() + timedelta(days=randint(1, 5))

    def change_status(self, status):
        self.status = status
    
    def change_priority(self, priority):
        self.priority = priority

    def as_dict(self):
        return attributes(self)

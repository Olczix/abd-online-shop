from website import initialize_database, prepare_database, app

initialize_database()
# prepare_database()
app = app

if __name__ == '__main__':
    app.run(debug=True)
from enum import IntEnum


# HELPERS
def attributes(obj):
    pr = {}
    for name in obj.__dict__.keys():
        if name[:1] != '_' and name != 'image':
            value = getattr(obj, name)
            pr[name] = value
    return pr

# Enums
class Category(IntEnum):
   TSHIRT = 1
   SHIRT = 2
   DRESS = 3
   TROUSERS = 4

class Roles(IntEnum):
   ADMIN = 1
   USER = 2

class Priority(IntEnum):
    LOW = 1
    MEDIUM = 2
    HIGH = 3
    URGENT = 4

class OrderStatus(IntEnum):
    SUBMITTED = 1
    IN_PROGRESS = 2
    IN_DELIVERY = 3
    DELIVERED = 4

def is_null(value):
    if value is None:
        return True 
    else:
        return False
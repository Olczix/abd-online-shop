import datetime
import random


def random_date():
    """
    This function will return a random datetime between two datetime
    objects.
    """
    start = datetime.date(year=2021, month=11, day=20)
    end = datetime.date(year=2021, month=11, day=29)
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + datetime.timedelta(seconds=random_second)
